title=Thank you for contributing to Adblock Plus!
prevent_cookie_prompt=1

<head>
  <meta name="robots" content="noindex">
<head>

{{ p1 On behalf of our team, we’d like to really thank you for supporting a product that you and millions of people use every day. Let’s make the internet an enjoyable experience for everyone, together! }}

{{ p2 You can see what we’re up to anytime by checking out [our blog](https://adblockplus.org/blog). }}

{{ p3 Questions about Adblock Plus? Reach out to us via email at <a data-mask='{"href": "bWFpbHRvOnN1cHBvcnRAYWRibG9ja3BsdXMub3Jn", "textContent": "c3VwcG9ydEBhZGJsb2NrcGx1cy5vcmc"}'>this address</a> or [send us a bug report](https://adblockplus.org/bugs#reporting). You can also check out our [Help Center](https://help.eyeo.com/). }}

<footer markdown=1>
  {{ p4 With love, <span class="block">Your friends at Adblock Plus</span> }}
</footer>

<script src="/js/address-masking.js"></script>
